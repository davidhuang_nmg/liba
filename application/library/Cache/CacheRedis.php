<?php

class CacheRedis extends Cache {
	protected $redis_master;
	protected $redis_slave;
	protected $redisServer;
	public $is_connected = false;
	protected $ttl = 0;
	protected $cacheable = false;
	protected $pk = false;
	protected $pkVal = false;
	
	protected $pkArray = array();
	
	public function __construct() {
		$this->connect();
	}

	public function __destruct() {
		$config = Yaf_Registry::get("config");
		$sessionType = $config->session->type;
		if (strtolower($sessionType) != "sessionredis") {
			$this->close(); // 在sessionRedis中最後會用到並close掉
		}
	}
	
	// return redis server instance
	public function getRedis($type="slave")
	{
		if ($this->is_connected) {
			return $type == "slave" ? $this->redis_slave : $this->redis_master;
		}
		return false;
	}
	
	public function connect() 
	{
		if ($this->is_connected)
			return true;
		if (extension_loaded('redis') && class_exists('Redis')) {
			$this->redis_master = new Redis();
		} else {
			return false;
		}
		$server = $this->getServerConfig();
		if (!$server)
			return false;
		
		if (!empty($server['slave_host']) && !empty($server['slave_port'])) {
			$this->redis_slave = new Redis();
		}

		G('cacheStartTime');
		$connectedMaster = $this->redis_master->connect($server['host'], $server['port']);
		if (!$connectedMaster)  return false;
		
		if ($this->redis_slave) {
			$connectedClient = $this->redis_slave->connect($server['slave_host'], $server['slave_port']);
		}
		$this->_countTime("conn cache", "get");

		G('cacheStartTime');
		if (!empty($server['password'])) {
			$connectedMaster = $this->redis_master->auth($server['password']);
		}

		if ($this->redis_slave && !empty($server['slave_password'])) {
			$connectedClient = $this->redis_slave->auth($server['slave_password']);
		}
		$this->_countTime("redis auth", "get");
		if (!$connectedMaster || ($this->redis_slave && !$connectedClient))  return false;
		
		$this->redis_master->select($server['db']);
		if ($this->redis_slave) {
			$this->redis_slave->select($server['db']);
		}
		$this->ttl = $server['ttl'];
		$this->is_connected = true;
		return true;
	}

	/**
	 * @param type 0:read,1:write
	 */
	protected function getRedisServer($type=0)
	{
		if (!$this->is_connected) return false;
		if ($type === 0) {
			$this->redisServer = $this->redis_slave ? $this->redis_slave : $this->redis_master;
		} else {
			$this->redisServer = $this->redis_master;
		} 
		return $this->redisServer;
	}

	protected function _set($key, $value, $ttl = 0) {
		if (!$this->is_connected)
			return false;
		if (is_array($value)) {
			$value = json_encode($value);
		} 
		if ($this->ttl > 0 && $ttl <= 0) {
			$ttl = $this->ttl;
		}
		G('cacheStartTime');
		$result = ($ttl > 0) ? $this->redis_master->setex($key, $ttl, $value) : 
					$this->redis_master->set($key, $value); 
		$this->_countTime("set {$key}", "set");
		return $result;
	}
	
	private function _countTime($log, $type) {
		if (BENCHMARK_MODE) {
			G('cacheEndTime');
			$leave = G("cacheStartTime", "cacheEndTime", 6);
			$GLOBALS['b_cache_total'][] = $leave;
			trace('[RunTime:' . $leave . 's] | ' . $log, $type . "_cache");
		}
	}
	
	protected function _get($key) {
		if (!$this->is_connected || !$key)
			return false;
		G('cacheStartTime');
		$value = $this->getRedisServer()->get($key);
		$this->_countTime("get " . $key, "get");
		$result = json_decode($value, true);
		return is_array($result) ? $result : $value;
	}
	
	protected function _getKeys($key)
	{
		G('cacheStartTime');
		$redis_server = $this->getRedisServer();
		$result = $redis_server ? $redis_server->keys($key) : false;
		$this->_countTime("get keys " . $key, "get");
		return $result;
	}
	
	/**
	 * @return boolean
	 */
	protected function _exists($key) {
		if (!$this->is_connected)
			return false;
		G('cacheStartTime');
		$result = $this->getRedisServer()->exists($key);
		$this->_countTime("exists " . $key, "get");
		return $result;
	}
	
	/**
	 * @param string or array string用逗號分隔
	 * @return Long Number of keys deleted
	 */
	protected function _delete($key) {
		if (!$this->is_connected)
			return false;
		G('cacheStartTime');
		$result = $this->getRedisServer(1)->delete($key);
		$this->_countTime("delete " . $key, "set");
		return $result;
	}

	protected function _writeKeys() {

	}

	public function flush() {
		if (!$this->is_connected)
			return false;
		G('cacheStartTime');
		$result = $this->getRedisServer(1)->flushDB();
		$this->_countTime("flush", "set");
		return $result;
	}
	
	// 无用
	protected function flushAll() {
		if (!$this->is_connected)
			return false;
		G('cacheStartTime');
		$result = $this->getRedisServer(1)->flushAll();
		$this->_countTime("flush all", "set");
		return $result;
	}

	public function close() {
		if (!$this->is_connected)
			return false;
		if ($this->getRedisServer()) 
			$this->getRedisServer()->close();
		if ($this->getRedisServer(1)) 
			$this->getRedisServer(1)->close();
		$this->is_connected = false;
		return true;
	}

	protected function getServerConfig() {
		if (Yaf_Registry::has('redis_server')) {
			return Yaf_Registry::get('redis_server');
		} else {
			$redis = Yaf_Registry::get('config')->cache->redis;
			$server = array();
			if (!empty($redis))	{
				$server = array('host' => $redis->master->host, 
								'port' => $redis->master->port, 
								'ttl'=>$redis->ttl, 
								"db"=>$redis->db);
				if (isset($redis->master->password)) {
					$server['password'] = $redis->master->password;
				}
				if (isset($redis->masterslave) && $redis->masterslave) {
					$slave_servers = isset($redis->slave) ? $redis->slave->host : false;
				
					if (!empty($slave_servers)) {
						$slaves = array();
						$slave_ports = explode('|', $redis->slave->port);
						$slave_passwords = explode('|', $redis->slave->passwords);
						$slave_servers = explode('|', $slave_servers);
						foreach ($slave_servers as $key => $slave_server) {
							$slaves[] = array('host' => $slave_server, 
											  'port' => (isset($slave_ports[$key]) && $slave_ports[$key]) ? $slave_ports[$key] : $server['port'], 
											  'password' => (isset($slave_passwords[$key]) && $slave_passwords[$key]) ? $slave_passwords[$key] : $server['password']);
						}
						
						$slave = $slaves[array_rand($slaves)];// 只執行一次	
						$server['slave_host'] = $slave['host'];
						$server['slave_port'] = $slave['port'];
						$server['slave_password'] = $slave['password'];
						$server['masterslave'] = true;
					}
				} else {
					$server['masterslave'] = false;
				}
				Yaf_Registry::set('redis_server', $server);
			}
			return $server;
		}
	}
	
	protected function getPrimaryKey($table){
        if(strpos($table,')')){
            // 子查询
			$this->pk = false;
            return false;
        }
		if (isset($this->pkArray[$table])) {
			$this->pk = $this->pkArray[$table];
		} else {
			$this->pk = Db::getInstance()->getPrimaryKey($table);
		    $this->pkArray[$table] = $this->pk;
		}        
		return $this->pk;
    }
	
	private function _delInvalidString($value)
	{
		$patterns[0] = "/'/";
		$patterns[1] = "/\"/";
		$patterns[2] = "/\(/";
		$patterns[3] = "/\)/";
		
		$replacements[0] = "";
		$replacements[1] = "";
		$replacements[2] = "";
		$replacements[3] = "";
		$value = preg_replace($patterns, $replacements, $value);
		return $value;
	}
	
	/**
	 * 查询条件是否包含主键=%d
	 **/
	protected function checkCacheAble($sql)
	{
		$this->pkVal = false;
		$this->cacheable = false;
		$patten = "/[\s]+/";  //正则格式，匹配多个空格
		$resArr = preg_split($patten, $sql);
		
		$pk = $this->pk;
		if (!$pk || empty($resArr) || !is_string($pk)) {
			$this->cacheable = false;
			return;
		}
		
		$newSql = implode(" ", $resArr);
		
		// 只处理int和英文字符的主键值
		preg_match('/[\(`\s]+'.$pk.'[`\s]*=[\'\"\s]*([\da-z0-9_]+)(([\'\"\s\)]+)|$)/i',$sql,$match);
		if (!empty($match) && isset($match[1]) && $match[1]) {
			$this->pkVal = $match[1];
			$this->cacheable = true;
		}		
	}
	
	protected function getQueryKey($sql, $nrows=false)
	{
		$tablename = $this->getTables($sql);
		if (!$tablename) return false;
		
		$tablename = implode("", $tablename);
		// 只缓存show和select类型的sql，select类型的sql需要查主键再确定是否需要缓存,
		$slim = Yaf_Registry::get('config')->cache->slim;
		if ($slim) {
			if (preg_match('#^\s*\(?\s*(select)\s#i', $sql)) {
				$this->getPrimaryKey($tablename);
				$this->checkCacheAble($sql);
			} elseif (!preg_match('#^\s*\(?\s*(show)\s#i', $sql)) {
				return false;
			} else {
				$this->cacheable = true;
				$this->pkVal = "show";
			}
			if (!$this->cacheable || !$this->pkVal) return false;
		}
		$table_prefix = Yaf_Registry::get('config')->cache->table_prefix;
		$tablename_prefix = $table_prefix . $tablename . ":" . ($this->pkVal ? ($this->pkVal . ":") : "");
		$key = $tablename_prefix . md5($sql) . ($nrows ? "_nrows" : "");
		return $key;
	}
	
	public function deleteQuery($query) {
		if ($tables = $this->getTables($query)) {
			foreach ($tables as $table)	{
				if (!empty($table)) {
					$slim = Yaf_Registry::get('config')->cache->slim;
					$table_prefix = Yaf_Registry::get('config')->cache->table_prefix;
					if ($slim) {
						$this->getPrimaryKey($table);
						$this->checkCacheAble($query);
						if (!$this->pk || !$this->pkVal) continue;
						$this->delete($table_prefix . $table . ":" . $this->pkVal . "*");
					} else {
						$this->delete($table_prefix . $table . "*");
						$this->delete($table_prefix . $table . "_nrows*");
					}					
				}
			}
		}
	}
	
	public function setQuery($query, $result, $nrows=false) {
		$key = $this->getQueryKey($query,$nrows);
		if (!$key) return false;
		$this->set($key, $result);
	}
}
