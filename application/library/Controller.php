<?php
    /*
     Name: Controller
     author: ccx
     Description: 扩展YAF控制器，增加常用方法以简化调用方式，如无必要请不要增加到此类内
     同控制器无关的常用函数可以放到autoload目录下的functions.php或以类的方式放到library下面自动加载
     */
    abstract class Controller extends Yaf_Controller_Abstract {
		/**
         * 也可通过$request->getException()获取到发生的异常
         */
        public function errorAction($exception) {
            $this->getView()->assign("code", $exception->getCode());
            $this->getView()->assign("message", $exception->getMessage());
			$this->getView()->setScriptPath(MODULE_PATH . "views/");
			return true;
        }
		
		/**
	     * 初始化，取得模板对象实例
	     * @access public
	     */
	    public function init() {
	        // //控制器初始化
	    	if(method_exists($this,'_initialize'))
	       		$this->_initialize();
        }
		
		/**
	     * 魔术方法 有不存在的操作的时候执行
	     * @access public
	     * @param string $method 方法名
	     * @param array $args 参数
	     * @return mixed
	     */
	    public function __call($method,$args) 
	    {
	        if( 0 === strcasecmp($method,ACTION_NAME . 'Action')) {
	            if(method_exists($this,'_empty')) {
	                // 如果定义了_empty操作 则调用
	                $this->_empty($method,$args);
	            }elseif(file_exists_case($this->view->parseTemplate())){
	                // 检查是否存在默认模版 如果有直接输出模版
	                $this->display();
	            }else{
	            	echo "Illegal Action:" . ACTION_NAME;
	            }
	        }else{
	        	echo $method. " The method you requested  does not exist!";
	            return;
	        }
	    }
	
        /**
         * 取URL参数，相当$_GET加上路由结果的参数，PHP默认$_GET只取?后面的查询字串
         *
         * 先试图获取路由结果的以/分隔的参数，如果找不到，就会尝试取?后面的参数，如果都没有返回默认值
         *
         * @param string $key
         * @param string $default
         * @return string
         */
        protected function GET($key, $default = '')
        {
            $request = $this->getRequest();
            
            if (array_key_exists($key, $request->getParams())) {
                return $request->getParam($key);
            }
            
            if (array_key_exists($key, $request->getQuery())) {
                return $request->getQuery($key);
            }
            
            return $default;
        }
        
        /**
         * 取POST参数,相当$_POST加上路由结果的参数
         *
         * 先试图获取路由结果的以/分隔的参数，如果找不到，就会尝试从$_POST取，如果都没有返回默认值
         *
         * @param string $key
         * @param string $default
         * @return string
         */
        protected function POST($key, $default = '')
        {
            $request = $this->getRequest();
            
            if (array_key_exists($key, $request->getParams())) {
                return $request->getParam($key);
            }
            
            if (array_key_exists($key, $request->getPost())) {
                return $request->getPost($key);
            }
            
            return $default;
        }
        
        /**
         * $_GET或$_POST的值，不要与框架本身的getRequest混淆
         *
         * 等同组合了get & post方法
         *
         * @param string $key
         * @param string $default
         * @return string
         */
        protected function REQUEST($key, $default = '')
        {
            $request = $this->getRequest();
            
            if (array_key_exists($key, $request->getParams())) {
                return $request->getParam($key);
            }
            
            if (array_key_exists($key, $request->getQuery())) {
                return $request->getQuery($key);
            }
            
            if (array_key_exists($key, $request->getPost())) {
                return $request->getPost($key);
            }
            
            return $default;
        }	
		
		/**
	     +----------------------------------------------------------
	     * Ajax方式返回数据到客户端，重组装BY davidhuang
	     +----------------------------------------------------------
	     * @access protected
	     +----------------------------------------------------------
	     * @param mixed $data 要返回的数据
	     * @param String $info 提示信息
	     * @param boolean $status 返回状态
	     +----------------------------------------------------------
	     * @return void
	     +----------------------------------------------------------
	     */
	    protected function response($status, $info='', $data=null)
	    {
			$result['status'] = $status;
			$result['info'] = $info;
			$result['data'] = $data;
			$this->ajaxReturn($result, "json");
	    }
		
		/**
	     * Ajax方式返回数据到客户端
	     * @access protected
	     * @param mixed $data 要返回的数据
	     * @param String $type AJAX返回数据格式
	     * @param int $json_option 传递给json_encode的option参数
	     * @return void
	     */
	    protected function ajaxReturn($data,$type='',$json_option=0) {
	        if(empty($type)) $type  =   "json";
			ob_clean();
	        switch (strtoupper($type)){
	            case 'JSON' :
	                // 返回JSON数据格式到客户端 包含状态信息
	                header('Content-Type:application/json; charset=utf-8');
	                exit(json_encode($data,$json_option));
	            case 'XML'  :
	                // 返回xml格式数据
	                header('Content-Type:text/xml; charset=utf-8');
	                exit(xml_encode($data));
	            case 'EVAL' :
	                // 返回可执行的js脚本
	                header('Content-Type:text/html; charset=utf-8');
	                exit($data);            
	            default     :
	                exit("unknow return type");
	        }
	    }
    }


?>
    
