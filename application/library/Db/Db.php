<?php

abstract class Db {

	const INSERT = 1;
	const INSERT_IGNORE = 2;
	const REPLACE = 3;
	
	protected $server;

	protected $user;

	protected $password;

	protected $database;
	
	protected $is_cache_enabled;

	protected $log_error;

	protected $link;
	
	protected $links = array();
	
	protected $slaveIndex;
	
	protected $result;

	protected static $instance = array();

	protected static $_servers = array();
	
	protected static $_fields = array();
	
	protected $last_query;
    
    protected $last_sql;

	protected $last_cached;
	
	protected $masterslave = false;
		
	abstract public function connect();

	abstract public function disconnect();

	abstract protected function _query($sql);

	abstract protected function _numRows($result);

	abstract public function Insert_ID();

	abstract public function Affected_Rows();

	abstract public function nextRow($result = false);

	abstract public function getVersion();

	abstract public function _escape($str);

	abstract public function getMsgError();

	abstract public function getNumberError();

	abstract public function set_db($db_name);
	
	
	
	/**
	 * 获取Db实例，可选择主从
	 *
	 * @param bool $master
	 * @param thirdIndex 從1開始
	 * @return mixed
	 */
	public static function getInstance($thirdIndex=false) {
		static $id = 0;
		if (self::$_servers == null) {
			self::$_servers = Yaf_Registry::get('database');
		}
				
		if (is_int($thirdIndex) && $thirdIndex >= 1) {
			if (!isset(self::$instance["third" . $thirdIndex]))
			{
				// 鏈接新的第三方DB之前要先釋放之前的第三方鏈接，否則可能造成內存洩露
				if (isset(self::$instance["third"])) {
					self::$instance["third"]->removeThirdInstance();
				}
				$class = Db::getClass();
				include_once(dirname(__FILE__) . '/' . $class . '.php');
				$thirdServers = isset(self::$_servers["third"]) ? self::$_servers["third"] : false;
				if (!empty($thirdServers) && isset($thirdServers[$thirdIndex-1])) {
					self::$instance["third"] = new $class($thirdServers[$thirdIndex-1]['server'], 
							$thirdServers[$thirdIndex-1]['user'], 
							$thirdServers[$thirdIndex-1]['password'], 
							$thirdServers[$thirdIndex-1]['database'], false);
					return self::$instance["third"];
				}
			} else {
				return self::$instance["third"];
			}
		} 
		
		// 如果已經連接了第三方數據庫，默認連接第三方數據庫。如果需要連回默認數據庫，請removeThirdInstance斷開第三方數據庫連接
		if (isset(self::$instance["third"])) {
			return self::$instance["third"];
		}
		
		$id_server = 0;
		if (!isset(self::$instance[$id_server])) {
			$class = Db::getClass();
			include_once(dirname(__FILE__) . '/' . $class . '.php');
			self::$instance[$id_server] = new $class(self::$_servers[$id_server]['server'], self::$_servers[$id_server]['user'], 
				self::$_servers[$id_server]['password'], self::$_servers[$id_server]['database'], false);
		}
		
		$databaseConfig = Yaf_Registry::get('config')->database;
		
		if (count(self::$_servers) > 1 && isset($databaseConfig->masterslave) && $databaseConfig->masterslave == true) {
			if (!isset(self::$instance[$id_server]->slaveIndex)) {
				self::$instance[$id_server]->masterslave = true;
				self::$instance[$id_server]->slaveIndex = array_rand(array(0, 1));
			}
		}
		return self::$instance[$id_server];
	}

	public function removeThirdInstance()
	{
		if (isset(self::$instance["third"])) {
			$this->disconnect();
			unset(self::$instance["third"]);
		}
	}

	public static function getClass() {
		$class = 'DbMySQLi';		
		return $class;
	}

	public function __construct($server, $user, $password, $database, $connect = true) {
		$this->server = $server;
		$this->user = $user;
		$this->password = $password;
		$this->database = $database;
		$this->is_cache_enabled = (defined('MYSQL_CACHE_ENABLE')) ? MYSQL_CACHE_ENABLE : false;
		$this->log_error = (defined('MYSQL_LOG_ERROR') ? MYSQL_LOG_ERROR : false);
		
		if ($connect) {
			$this->connect();
			$this->setLink();
		}
	}
	
	/**
	 * set  master or slave link
	 */
	protected function setLink($link=false, $server=false)
	{
		if ($this->masterslave) {
			$currLink = $link ? $link : $this->link;
			$currServer = $server ? $server : $this->server;
			if ($currServer == self::$_servers[0]['server']) {
				$this->links[0] = $currLink;
			} else {
				$this->links[1] = $currLink;
			}
		}
	}
	
	/**
	 * get master or slave link
	 */
	protected function getLink($linkIndex)
	{
		$this->link = $this->links[$linkIndex];
	}

	public function __destruct() {
		if ($this->link) {
			if ($this->masterslave) {
				foreach ($this->links as $key => $link) {
					$this->link = $link;
					$this->disconnect();
				}
			} else {
				$this->disconnect();
			}
		}
	}
	
	protected function reconnect() {
		if ($this->masterslave) {
			if (count($this->links) > 1) return true;
			
			$servers = self::$_servers;
			foreach ($servers as $key => $server) {
				$this->server = $server['server'];
				$this->user = $server['user'];
				$this->password = $server['password'];
				$this->database = $server['database'];
				// slave和尚未connect的master，才connect
				if ($this->slaveIndex !== 0 || ($this->slaveIndex===0 && !$this->link)) {
					$this->connect();
				}
				if ($this->slaveIndex === 0 && $key===1) {
					$this->setLink(false, "master_link_mapping");
				} else {
					$this->setLink();
				}
			} 
		} else {
			if (!$this->link) {
				$this->connect();
			}
		}
	}
	
	public function query($sql) {
		$this->reconnect();
		if ($sql instanceof DbQuery)
			$sql = $sql->build();

		$this->result = $this->_query($sql);
		if ($this->log_error) {
			$this->logError($sql);
		}
        $this->last_sql = $sql;
		return $this->result;
	}

	/**
	 * 插入
	 *
	 * @param      $table
	 * @param      $data
	 * @param bool $null_values
	 * @param bool $use_cache
	 * @param int  $type
	 *
	 * @return bool
	 */
	public function insert($table, $data, $null_values = false, $use_cache = true, $type = Db::INSERT) {
		if (!$data && !$null_values)
			return true;

		if ($type == Db::INSERT)
			$insert_keyword = 'INSERT';
		else if ($type == Db::INSERT_IGNORE)
			$insert_keyword = 'INSERT IGNORE';
		else if ($type == Db::REPLACE)
			$insert_keyword = 'REPLACE';
		else
			die(Tools::displayError('SQL Error'));

		// Check if $data is a list of row
		$current = current($data);
		if (!is_array($current) || isset($current['type']))
			$data = array($data);

		$keys = array();
		$values_stringified = array();
		foreach ($data as $row_data)
		{
			$values = array();
			foreach ($row_data as $key => $value)
			{
				if (isset($keys_stringified))
				{
					// Check if row array mapping are the same
					if (!in_array("`$key`", $keys))
						die(Tools::displayError('Keys form $data subarray don\'t match'));
				}
				else
					$keys[] = "`$key`";

				if (!is_array($value)) {
					$value = array('type' => 'text', 'value' => pSQL($value));
				}
					
				if ($value['type'] == 'sql') {
					$values[] = $value['value'];
				} else {
					$values[] = $null_values && ($value['value'] === '' || is_null($value['value'])) ? 'NULL' : ("'" . $value['value'] . "'");
				}
			}
			$keys_stringified = implode(', ', $keys);
			$values_stringified[] = '(' . implode(', ', $values) . ')';
		}

		$sql = $insert_keyword . ' INTO `' . $table . '` (' . $keys_stringified . ') VALUES ' . implode(', ', $values_stringified);

		return (bool)$this->q($sql, $use_cache);
	}

	/**
	 * update
	 *
	 * @param        $table
	 * @param        $data
	 * @param string $where
	 * @param int    $limit
	 * @param bool   $null_values
	 * @param bool   $use_cache
	 *
	 * @return bool
	 */
	public function update($table, $data, $where = '', $limit = 0, $null_values = false, $use_cache = true) {
		if (!$data)
			return true;
		
		$sql = 'UPDATE `' . $table . '` SET ';
		foreach ($data as $key => $value)
		{
			if (!is_array($value)) {
				$value = array('type' => 'text', 'value' => $value);
			}
				
			if ($value['type'] == 'sql')
				$sql .= "`$key` = " . pSQL($value['value']) . ","; // add sql filter
			else
				$sql .= ($null_values && ($value['value'] === '' || is_null($value['value']))) ? 
				"`$key` = NULL," : ("`$key` = '" . pSQL($value['value']) . "',");
		}
		$sql = rtrim($sql, ',');
		if ($where)
			$sql .= ' WHERE ' . $where;
		if ($limit)
			$sql .= ' LIMIT ' . (int)$limit;

		return (bool)$this->q($sql, $use_cache);
	}

	/**
	 * delete
	 *
	 * @param        $table
	 * @param string $where
	 * @param int    $limit
	 * @param bool   $use_cache
	 *
	 * @return bool
	 */
	public function delete($table, $where = '', $limit = 0, $use_cache = true) {

		$this->result = false;
		$sql = 'DELETE FROM `' . pSQL($table) . '`' . ($where ? ' WHERE ' . $where : '') . ($limit ? ' LIMIT ' . (int)$limit : '');
		$res = $this->query($sql);
		if ($use_cache && $this->is_cache_enabled) {
			Cache::getInstance()->deleteQuery($sql);
		}
		return (bool)$res;
	}

	/**
	 * insert/delete/update
	 *
	 * @param      $sql
	 * @param bool $use_cache
	 *
	 * @return bool
	 */
	public function execute($commit_sql, $use_cache = true) {
		if ($commit_sql instanceof DbQuery)
			$sql = $commit_sql->build();
		else 
			$sql = $commit_sql; 
		
		$this->result = $this->query($sql);
		if ($use_cache && $this->is_cache_enabled) {
			Cache::getInstance()->deleteQuery($sql);
		}
		if ($commit_sql instanceof DbQuery) $commit_sql->reset();
		return (bool)$this->result;
	}
	
	/**
	 * 检查sql是否查询语句
	 */
	protected function checkQuerySql($sql)
	{
		return preg_match('#^\s*\(?\s*(select|show|explain|describe|desc)\s#i', $sql);
	}

	/**
	 * 执行select操作
	 *
	 * @param      $sql
	 * @param bool $array
	 * @param bool $use_cache
	 *
	 * @return array|bool|mixed
	 */
	public function executeS($commit_sql, $array = true, $use_cache = true) {
		if ($commit_sql instanceof DbQuery) {
			$sql = $commit_sql->build();
		} else {
			$sql = $commit_sql;
		}
		
		// This method must be used only with queries which display results
		if (!$this->checkQuerySql($sql)) {
			if ($commit_sql instanceof DbQuery) $commit_sql->reset();
			return $this->execute($sql, $use_cache);
		}
		
		$this->result = false;
		$this->last_query = $sql;
		
		if ($use_cache && $this->is_cache_enabled && $array)
		{
			$result = Cache::getInstance()->getQuery($sql);
			if ($result || is_array($result) || is_string($result)) {
				$this->last_cached = true;
	            $this->last_sql = $sql;
				if ($commit_sql instanceof DbQuery) $commit_sql->reset();
				return $result;
			}
		}
		$this->reconnect();
		$this->result = $this->query($sql);
		if (!$this->result)
			return false;
		$this->last_cached = false;
		if (!$array) {
			if ($commit_sql instanceof DbQuery) $commit_sql->reset();
			return $this->result;
		}
		$result_array = array();
		while ($row = $this->nextRow($this->result))
			$result_array[] = $row;
		if ($use_cache && $this->is_cache_enabled) {
			Cache::getInstance()->setQuery($sql, $result_array);
		}
			
		if ($commit_sql instanceof DbQuery) $commit_sql->reset();
		return $result_array;
	}

	/**
	 * 限制select一行数据
	 *
	 * @param      $sql
	 * @param bool $use_cache
	 *
	 * @return bool|mixed
	 */
	public function getRow($commit_sql, $use_cache = true) {
		if ($commit_sql instanceof DbQuery) {
			$sql = $commit_sql->build();
		}  else {
			$sql = $commit_sql;
		}
		if (stripos($sql, ' limit ') === false) {
			$sql .= ' LIMIT 1';
		}
		$this->result = false;
		$this->last_query = $sql;
		if ($use_cache && $this->is_cache_enabled) {
			$result = Cache::getInstance()->getQuery($sql);
			if ($result || is_array($result) || is_string($result)) {
				$this->last_cached = true;
	            $this->last_sql = $sql;
				if ($commit_sql instanceof DbQuery) $commit_sql->reset();
				return $result;
			}
		}
		
		$this->result = $this->query($sql);
		if (!$this->result) {
			if ($commit_sql instanceof DbQuery) $commit_sql->reset();
			return false;
		}
		$this->last_cached = false;
		$result = $this->nextRow($this->result);
		if ($use_cache && $this->is_cache_enabled) {
			Cache::getInstance()->setQuery($sql, $result);
		}
			
		if ($commit_sql instanceof DbQuery) $commit_sql->reset();
		return $result;
	}

	/**
	 * 获取值
	 *
	 * @param      $sql
	 * @param bool $use_cache
	 *
	 * @return bool|mixed
	 */
	public function getValue($sql, $use_cache = true) {
		if (!$result = $this->getRow($sql, $use_cache))
			return false;

		return array_shift($result);
	}

	/**
	 * 查询结果行数
	 *
	 * @return bool|mixed
	 */
	public function numRows() {
		if (!$this->last_cached && $this->result)
		{
			$nrows = $this->_numRows($this->result);
			if ($this->is_cache_enabled) {
				Cache::getInstance()->setQuery($this->last_query, $nrows, '_nrows');
			}
			return $nrows;
		} else if ($this->is_cache_enabled && $this->last_cached) {
			$result = Cache::getInstance()->getQuery($this->last_query, '_nrows');
			return $result;
		}
		return 0;
	}

	protected function q($sql, $use_cache = true) {
		if ($sql instanceof DbQuery)
			$sql = $sql->build();

		$this->result = false;
		$result = $this->query($sql);
		if ($use_cache && $this->is_cache_enabled) {
			Cache::getInstance()->deleteQuery($sql);
		}
		return $result;
	}

	public function escape($string, $html_ok = false) {
		if (get_magic_quotes_gpc())
			$string = stripslashes($string);
		if (!is_numeric($string))
		{
			$this->reconnect();
			$string = $this->_escape($string);
			if (!$html_ok)
				$string = strip_tags(Tools::nl2br($string));
		}

		return $string;
	}

	public function nextId($table_name) {
		return $this->getValue("select AUTO_INCREMENT from information_schema.TABLES where TABLE_SCHEMA='" . $this->database . "' and TABLE_NAME='" . pSQL($table_name) . "'");
	}

	protected function logError($sql = false) {
		if ($this->getNumberError())
		{
			Log::out('sql_error', 'E', $this->getMsgError() . ':' . $sql);
		}
	}
    
    public function getLastSql() {
        return $this->last_sql;
    }

	public static function checkConnection($server, $user, $pwd, $db, $new_db_link = true, $engine = null, $timeout = 5) {
		return call_user_func_array(array(Db::getClass(), 'tryToConnect'), array($server, $user, $pwd, $db, $new_db_link, $engine, $timeout));
	}

	public static function checkEncoding($server, $user, $pwd) {
		return call_user_func_array(array(Db::getClass(), 'tryUTF8'), array($server, $user, $pwd));
	}

	public static function hasTableWithSamePrefix($server, $user, $pwd, $db, $prefix) {
		return call_user_func_array(array(Db::getClass(), 'hasTableWithSamePrefix'), array($server, $user, $pwd, $db, $prefix));
	}
	
	public function prepare($query, $args) {
		if ( is_null( $query ) )
			return;
		// This is not meant to be foolproof -- but it will catch obviously incorrect usage.
		if ( strpos( $query, '%' ) === false ) {
			die(sprintf(Tools::displayError('The query argument should not be null.')));
		}

		$args = func_get_args();
		array_shift( $args );
		// If args were passed as an array (as in vsprintf), move them up
		if ( isset( $args[0] ) && is_array($args[0]))
			$args = $args[0];
		$query = str_replace( "'%s'", '%s', $query ); // in case someone mistakenly already singlequoted it
		$query = str_replace( '"%s"', '%s', $query ); // doublequote unquoting
		$query = preg_replace( '|(?<!%)%f|' , '%F', $query ); // Force floats to be locale unaware
		$query = preg_replace( '|(?<!%)%s|', "'%s'", $query ); // quote the strings, avoiding escaped strings like %%s
		array_walk( $args, 'pSQL');
		return @vsprintf( $query, $args );
	}
	
	/**
     * 取得数据表的字段信息 update by david
     * @access public
     */
    public function getFields($tableName) {
    	if (!empty(self::$_fields[$tableName])) {
    		return self::$_fields[$tableName];
    	}
        list($tableName) = explode(' ', $tableName);
        $sql   = 'SHOW COLUMNS FROM `'.$tableName.'`';
        $result = $this->executeS($sql, true);
        $info   =   array();
        if($result) {
            foreach ($result as $key => $val) {
            	$val = array_change_key_case ( $val ,  CASE_LOWER );
                $info[$val['field']] = array(
                    'name'    => $val['field'],
                    'type'    => $val['type'],
                    'notnull' => (bool) ($val['null'] === ''), // not null is empty, null is yes
                    'default' => $val['default'],
                    'primary' => (strtolower($val['key']) == 'pri'),
                    'autoinc' => (strtolower($val['extra']) == 'auto_increment'),
                );
            }
        }
		self::$_fields[$tableName] = $info;
        return $info;
    }
	
	public function getPrimaryKey($tableName)
	{
		if (!$tableName) return false;
		
        $sql   = 'SHOW KEYS FROM `'. $tableName . '` WHERE Key_name=\'PRIMARY\'';
		$result = Cache::getInstance()->getQuery($sql);
		if ($result || is_array($result) || is_string($result)) {
			return $result;
		}
		
        $result = $this->_query($sql);
		if (!$result) return false;
		
		$row = $this->nextRow($result);
   		$pk = isset($row['Column_name']) ? $row['Column_name'] : false;
		if ($this->is_cache_enabled) {
			Cache::getInstance()->setQuery($sql, $pk);
		}
		return $pk;
	}
	
	/**
     * 数据库调试 记录当前SQL
     * @access protected
     */
    protected function debug($label, $sql=false) {
        // 记录操作结束时间
        if (BENCHMARK_MODE) {
            G($label . 'EndTime');
			$sql = $sql ? $sql : $this->last_sql;
            trace('[RunTime:'.G($label . 'StartTime', $label . 'EndTime', 6).'s] | ' . $sql, $label);
        }
    }
	
	public function isConnected() {
	    $ret = false;
		if ($this->masterslave) {
			if (!empty($this->links) && count($this->links) > 1) 
				$ret = true;
		} else {
			if (!empty($this->link)) 
				$ret = true;
		}
	    
	    return $ret;
	}
}
