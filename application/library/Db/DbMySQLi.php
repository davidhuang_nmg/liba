<?php

class DbMySQLi extends Db {
	public function connect() {
		G('sqlStartTime');
		if (strpos($this->server, ':') !== false) {
			list($server, $port) = explode(':', $this->server);
			$this->link = @new mysqli($server, $this->user, $this->password, $this->database, $port);
		}else
			$this->link = @new mysqli($this->server, $this->user, $this->password, $this->database);
		// Do not use object way for error because this work bad before PHP 5.2.9
		if (mysqli_connect_error()) {
			$errorInfo = iconv('gbk', 'utf-8', mysqli_connect_error());
			die(sprintf(Tools::displayError('Link to database cannot be established: %s'), $errorInfo));
		}
		// UTF-8 support
		if (!$this->link->query('SET NAMES \'utf8mb4\''))
			die(Tools::displayError('PrestaShop Fatal error: no utf-8 support. Please check your server configuration.'));
		$this->debug("sql", "connect db");
		return $this->link;
	}

	public function disconnect() {
		if (@$this->link->thread_id) {
			@$this->link->close();
		}
	}

	public function nextRow($result = false) {
		if (!$result)
			$result = $this->result;

		return $result->fetch_assoc();
	}

	public function Insert_ID() {
		$this->reconnect();
		G('sqlStartTime');
		$result = $this->link->insert_id;
		$this->debug('sql', "get insert id");
		return $result;
	}

	public function Affected_Rows() {
		$this->reconnect();
		G('sqlStartTime');
		$result = $this->link->affected_rows;
		$this->debug('sql', "get affects rows");
		return $result;
	}

	public function getMsgError($query = false) {
		$this->reconnect();
		G('sqlStartTime');
		$result = $this->link->error;
		$this->debug('sql', "get msg error");
		return $result;
	}

	public function getNumberError() {
		$this->reconnect();
		//G('sqlStartTime');
		$result = $this->link->errno;
		//$this->debug('sql', "get number error");
		return $result;
	}

	public function getVersion() {
		return $this->getValue('SELECT VERSION()');
	}

	public function _escape($str) {
		$this->reconnect();
		G('sqlStartTime');
		$result = $this->link->real_escape_string($str);
		$this->debug('sql', "string escape");
		return $result;
	}

	public function set_db($db_name) {
		$this->reconnect();
		G('sqlStartTime');
		$result = $this->link->query('USE ' . pSQL($db_name));
		$this->debug('sql', "use db " . $db_name);
		return $result;
	}

	public static function hasTableWithSamePrefix($server, $user, $pwd, $db, $prefix) {
		$link = @new mysqli($server, $user, $pwd, $db);
		if (mysqli_connect_error())
			return false;

		$sql = 'SHOW TABLES LIKE \'' . $prefix . '%\'';
		G('sqlStartTime');
		$result = $link->query($sql);
		$this->debug('sql', $sql);
		return (bool)$result->fetch_assoc();
	}

	public static function tryToConnect($server, $user, $pwd, $db, $newDbLink = true, $engine = null, $timeout = 5) {
		$link = mysqli_init();
		if (!$link)
			return -1;

		if (!$link->options(MYSQLI_OPT_CONNECT_TIMEOUT, $timeout))
			return 1;

		if (!$link->real_connect($server, $user, $pwd, $db))
			return (mysqli_connect_errno() == 1049) ? 2 : 1;

		if (strtolower($engine) == 'innodb')
		{
			$sql = 'SHOW VARIABLES WHERE Variable_name = \'have_innodb\'';
			G('sqlStartTime');
			$result = $link->query($sql);
			$this->debug('sql', $sql);
			if (!$result)
				return 4;
			$row = $result->fetch_assoc();
			if (!$row || strtolower($row['Value']) != 'yes')
				return 4;
		}
		$link->close();

		return 0;
	}

	public static function checkCreatePrivilege($server, $user, $pwd, $db, $prefix, $engine) {
		$link = @new mysqli($server, $user, $pwd, $db);
		if (mysqli_connect_error())
			return false;
		G('sqlStartTime');
		$sql = '
		CREATE TABLE `' . $prefix . 'test` (
		`test` tinyint(1) unsigned NOT NULL
		) ENGINE=MyISAM';
		$result = $link->query($sql);
		$this->debug('sql', $sql);
		
		if (!$result)
			return $link->error;
		
		G('sqlStartTime');
		$link->query('DROP TABLE `' . $prefix . 'test`');
		$this->debug('sql', "drop table `" . $prefix . "test`");
		return true;
	}

	static public function tryUTF8($server, $user, $pwd) {
		G('sqlStartTime');
		$link = @new mysqli($server, $user, $pwd);
		$ret = $link->query("SET NAMES \'utf8mb4\'");
		$link->close();
		$this->debug('sql', "try utf8");
		return $ret;
	}

	protected function _query($sql) {
		$this->reconnect();
        // 记录开始执行时间
        G('sqlStartTime');
		$linkIndex = 0;
		if ($this->masterslave) {
			$linkIndex = $this->checkQuerySql($sql) ? $this->slaveIndex : 0;
			$this->getLink($linkIndex);
		}
		$result = $this->link->query($sql);
		$this->debug('sql', $linkIndex . " | " . $sql);
		return $result;
	}

	protected function _numRows($result) {
		return $result->num_rows;
	}    
}
