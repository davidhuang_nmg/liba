<?php
Yaf_Loader::import(APPLICATION_PATH . '/modules/Admin/ORG/Nmg/SysLogs.php');
Yaf_Loader::import(APPLICATION_PATH . '/modules/Admin/ORG/Nmg/Auth.php');
class EntryController extends Controller {
    public function _initialize()
    {
    	 $user_auth_key = session(USER_AUTH_KEY);
		if(!isset($user_auth_key) && CONTROLLER_NAME != 'Auth') {
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
				$this->response(FALSE, 'timeout', '/Admin/Auth/index');
			} else {
				redirect('/Admin/Auth/index');
			}
			exit;
		}
		if (!\Org\Nmg\Auth::AccessDecision()){
            echo '沒有權限';
            exit;
        }
		
		//記錄系統日誌	
		\Org\Nmg\SysLogs::log();
    }

    public function showMsg($message,$jumpUrl='',$status=1,$waitSecond=3,$closeWin=1){
    	$this->getView()->assign("message", $message);
        $this->getView()->assign("waitSecond", $waitSecond);
        $this->getView()->assign("jumpUrl", $jumpUrl);
        $this->getView()->assign("closeWin",$closeWin);
        if($status){
        	$tmpl='auth/success.html';
        }else{
        	$tmpl='auth/showerror.html';
        }
        $this->getView()->display($tmpl);
    }
}

?>