<?php

class File {

    protected static $instance;

    public static function getInstance() {
	if (!self::$instance) {
	    $file_system = Yaf_Registry::get('config')->file->file_system;
	    if (!empty($file_system) && file_exists(dirname(__FILE__) . '/' . $file_system . '.php')) {
		Yaf_Loader::import(dirname(__FILE__) . '/' . $file_system . '.php');
		self::$instance = new $file_system();
	    }
	}

	return self::$instance;
    }

    public function get($filekey) {
	$sql = 'fileKey = "%s"';
	$row = M('Files')->where($sql, array($filekey))->getRow();
	return $row;
    }

    public function save() {
	$return = $this->uploadfiles();
	return $return;
    }

    public function del($filekey) {

    }

    /**
     * 圖片或文件上傳函數
     *
     * $this->fileInput 表單需要用數組 例如 photo[0] 或者是http://網址
     *
     * @return array 返回已上傳的圖片名稱數組
     */
    protected function uploadfiles() {
	$return = array();
	if (strstr($this->fileInput, "http://") || strstr($this->fileInput, "https://")) {
	    $return = $this->uploadHttpPic();
	} else {
	    if ($_FILES[$this->fileInput]) {
		foreach ($_FILES[$this->fileInput]["error"] as $key => $error) {
		    if ($error == UPLOAD_ERR_OK) {
			$return = $this->uploadLocalPic($key);
		    }
		}
	    }
	}
	return $return;
    }

    protected function uploadLocalPic($key) {
	$return = array();
	$return[$key]['code'] = 0;
	$return[$key]['msg'] = UPLOAD_PIC_FAILED;
	if (empty($_FILES[$this->fileInput]['size'][$key])) {
	    //$return[$key]['code'] = 2;
	    //$return[$key]['msg'] = '';
	    $return = array();
	    return $return;
	}

	$f_name = $_FILES[$this->fileInput]['name'][$key]; //獲取上傳源文件名
	$fileType = strtolower(substr(strrchr($f_name, "."), 1)); //獲取文件擴展名
	if (!strstr($this->fileType, $fileType)) {
	    $return[$key]['msg'] = FILETYPE_NOT_SUPPORT;
	    return $return;
	} if ($_FILES[$this->fileInput]['size'][$key] > $this->fileMaxSize || $_FILES[$this->fileInput]['size'][$key] == 0) {
	    $return[$key]['msg'] = OVER_FILE_MAX_SIZE . ($this->fileMaxSize / 1024 / 1024) . "MB";
	    return $return;
	}
	$size = getimagesize($_FILES[$this->fileInput]['tmp_name'][$key]);
	if (empty($size))
	    return $return;

	$is_width = 0;
	if (empty($this->no_min_width) && $fileType <> 'gif')
	    $is_width = 1;

	if ($size[0] < $this->min_width && $is_width) {
	    $return[$key]['msg'] = PIC_LESS_WIDTH;
	} elseif (($size[0] / $size[1] < $this->ratio && $is_width ) || ($size[1] / $size[0] < $this->ratio && $is_width )) {
	    $return[$key]['msg'] = PIC_LESS_RATIO;
	} else {
	    $ret = $this->upload($this->uploadDir, $key, $fileType, $this->fileInput);
	    if ($ret['code'] == 1) {

		$return[$key]['code'] = $ret['code'];
		$return[$key]['filename'] = $f_name;
		$return[$key]['path'] = $ret['path'];
		$return[$key]['fileKey'] = $ret['fileKey'];
		$return[$key]['fileType'] = $fileType;
		$return[$key]['msg'] = 'ok';
	    } else {
		$return[$key]['code'] = $ret['code'];
		$return[$key]['msg'] = $ret['msg'];
	    }
	}
	return $return;
    }

    protected function uploadHttpPic() {
	$return = array();
	$return[0]['code'] = 0;
	$return[0]['msg'] = FETCH_PIC_FAILED;
	$Curl = new Curl();
	$Curl->https = 1;
	$fileData = $Curl->fetchUrl($this->fileInput);
	if (!empty($fileData)) {
	    $url = parse_url($this->fileInput);
	    $fileType = strtolower(substr(strrchr($url['path'], "."), 1)); //獲取文件擴展名
	    $ret = $this->upload($this->uploadDir, 0, $fileType, $fileData, 1);
	    if ($ret['code'] == 1) {
		$return[0]['code'] = $ret['code'];
		$return[0]['msg'] = 'ok';
		$return[0]['filename'] = basename($this->fileInput);
		$return[0]['path'] = $ret['path'];
		$return[0]['fileKey'] = $ret['fileKey'];
	    } else {
		$return[0]['code'] = $ret['code'];
		$return[0]['msg'] = $ret['msg'];
	    }
	}
	return $return;
    }

    protected function getFilePath($uploadDir, $id, $fileType) {
	if (empty($this->fileName)) {
	    $pic_path = $uploadDir . '/' . date('Y') . '/' . date('m') . '/' . date('d');
	    $uploadfile_path = $pic_path . '/' . $id . '_' . date("YmdHis") . '_' . uniqid() . '.' . $fileType;
	} else {

	    $this->fileName = filePath($this->fileName);
	    $uploadfile_path = ROOT_PATH . "/public/" . $this->fileName;
	    $pic_path = ROOT_PATH . "/public/" . dirname($this->fileName);
	}

	createDirs($pic_path);

	return $uploadfile_path;
    }

    protected function addFilesDB() {
	$attr = array();
	$attr['addtime'] = dateTime();
	$attr['fileKey'] = uniqid();
	$attr['filePath'] = '';
	$attr['rotateParam'] = '';
	$attr['addQueue'] = 3;
	$attr['isIM'] = $this->isIM;
	if (empty($this->fileName)) {
	    $id = M('Files')->add($attr);
	    $attr['id'] = $id;
	} else {
	    $attr['id'] = $this->fileID;
	}
	return $attr;
    }

    protected function uploadFileInput($isDownload, $uploadfile_path, $fileInput, $key) {
	$isUploaded = 0;
	if ($isDownload) {
	    if (file_put_contents($uploadfile_path, $fileInput))
		$isUploaded = 1;
	} else {

	    if (move_uploaded_file($_FILES[$fileInput]['tmp_name'][$key], $uploadfile_path))
		$isUploaded = 1;
	}
	if (!empty($isDownload))
	    @chmod($uploadfile_path, 0777);
	return $isUploaded;
    }

    protected function changeDirection($uploadfile_path) {
	$rotateParam = '';
	$exif = @exif_read_data($uploadfile_path);

	if (!empty($exif['Orientation'])) {
	    switch ($exif['Orientation']) {
		case 8:
		    $rotateParam = "filters:rotate(90)/";
		    break;
		case 3:
		    $rotateParam = "filters:rotate(180)/";
		    break;
		case 6:
		    $rotateParam = "filters:rotate(-90)/";
		    break;
	    }
	}
	return $rotateParam;
    }

}
