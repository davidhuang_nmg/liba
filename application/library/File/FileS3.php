<?php

/**
 * @name   S3File
 * @author denis
 * @desc   默认控制器
 */
class FileS3 extends File {

    protected $uploadDir = '';
    protected $fileInput = '';
    protected $fileType = '';
    protected $fileMaxSize = 0;

    public function __construct($fileInput = '', $no_min_width = 0, $fileID = '', $path = '', $isIM = 0, $uploadDir = '', $fileType = '', $fileMaxSize = 0, $min_width = 0, $ratio = 0) {
	$file = Yaf_Registry::get('config')->file;
	$this->uploadDir = empty($uploadDir) ? $file->dir : $uploadDir;
	$this->isIM = empty($isIM) ? 0 : $isIM;
	$this->fileInput = empty($fileInput) ? $file->fileInput : $fileInput;
	$this->fileType = empty($fileType) ? $file->file_type : $fileType;
	$this->fileMaxSize = empty($fileMaxSize) ? $file->max_size : $fileMaxSize;
	$this->min_width = empty($min_width) ? $file->min_width : $min_width;
	$this->ratio = empty($ratio) ? $file->ratio : $ratio;
	$this->no_min_width = $no_min_width;
	$this->fileID = $fileID;
	$this->fileRootDir = ROOT_PATH . "/public";
	$this->fileName = '';
	if (!empty($fileID)) {
	    $file = M("Files")->where('id = %d and status = 1', array($fileID))->find();
	    $this->fileName = $file['filePath'];
	}
	if (!empty($path))
	    $this->fileName = $path;
    }

    protected function upload($uploadDir, $key, $fileType, $fileInput, $isDownload = 0) {
	$attr = $this->addFilesDB();
	$id = $attr['id'];
	$return = array();

	$uploadfile_path = $this->getFilePath($uploadDir, $id, $fileType);
	$isUploaded = $this->uploadFileInput($isDownload, $uploadfile_path, $fileInput, $key);

	if ($isUploaded) {
	    $rotateParam = $this->changeDirection($uploadfile_path);

	    $filePath = str_replace($this->fileRootDir . '/', "", $uploadfile_path);

	    /*
	     * S3 start
	     */
	    require_once APPLICATION_PATH . '/library/aws-autoloader.php';
	    $s3 = Aws\S3\S3Client::factory(['credentials' => array('key' => Yaf_Registry::get('config')->file->aws_key, 'secret' => Yaf_Registry::get('config')->file->aws_secret),
			'region' => Yaf_Registry::get('config')->file->aws_region, 'version' => 'latest']);

	    $key = $filePath;
	    $setting = Yaf_Registry::get('config')->file->aws_setting;
	    if (!empty($setting))
		$key = $setting . '/' . $filePath;
	    $result = $s3->putObject(['Bucket' => Yaf_Registry::get('config')->file->aws_bucket, 'Key' => $key, 'Body' => fopen($uploadfile_path, 'rb'), 'ACL' => 'public-read']);
	    /*
	     * S3 end
	     */

	    $isS3 = 0;
	    if ($result) {
		$isS3 = 1;
		@unlink($uploadfile_path);
	    }
	    $saveArray = array();
	    $saveArray['filePath'] = $filePath;
	    $saveArray['isS3'] = $isS3;
	    if (empty($this->fileName)) {
		$saveArray['addQueue'] = 0;
		$saveArray['rotateParam'] = $rotateParam;
	    }
	    M("Files")->where("id='%d'", array($id))->save($saveArray);

	    $return['code'] = 1;
	    $return['path'] = $filePath;
	    $return['fileKey'] = $attr['fileKey'];
	} else {
	    $return['code'] = 0;
	    $return['msg'] = UPLOAD_PIC_FAILED;
	    M("Files")->where("id='%d'", array($id))->save(array('status' => 0));
	}
	return $return;
    }

}