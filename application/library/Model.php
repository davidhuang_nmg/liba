<?php
    /*
     Name: Model
     author: ccx create,base on other lib
     Description:
     */
    class Model
    {
        protected $dbQuery;
        
        protected $tablename = null;
        // 实际数据表名（包含表前缀）
    	protected $trueTableName    =   '';
        protected $restriction = array();
		
        // 字段信息 update by david
    	protected $fields           =   array();
		protected $pk = "id"; // primary key, update by david
		protected $autoinc = false; // auto increace field, update by david
		
        function __construct($tablename = null)
        {
        	// 模型初始化
        	$this->_initialize();
            $classname = get_class($this);
            
            if ('Model' != $classname) {
                $tablename = substr($classname, 0, strlen($classname ) - 5);
            }
            
            $tablename = lcfirst($tablename);
            
            $this->dbQuery = new DbQuery();
            if ($this->trueTableName) {
				$tablename = $this->trueTableName;
			}
            if (!empty($tablename)) {
                $this->tablename = $tablename;
                $this->dbQuery->from($tablename);
            }
			
        }
        
		// 回调方法 初始化模型
    	protected function _initialize() {}
				
		/**
		 * @param data
		 * @return mixed insert failed return false,autoinc return insertid,other return true
		 */
        public function add($datas)
        {
            if (is_null($this->tablename)) return false;
            $datas       =   $this->_facade($datas);
			if (!$datas) return false;
            $result = Db::getInstance()->insert($this->tablename, $datas);
			$this->_reset();
            if ($result) {
            	$insertID = Db::getInstance()->Insert_ID();
				if ($insertID && $this->autoinc) return $insertID;// 自增主键返回插入ID
                return $result;
            }
            return false;
        }
        
		/**
		 * @return boolean
		 */
        public function save($datas)
        {
            if (is_null($this->tablename)) return false;
            $datas       =   $this->_facade($datas);
			if (!$datas) return false;
            //禁止无条件的保存操作
            $where = ' 0 = 1 ';
            if ($this->restriction)
                $where = ' (' . implode(') AND (', $this->restriction) . ")\n";
            
            $result = Db::getInstance()->update($this->tablename, $datas, $where);

            $this->_reset();
			return $result;
        }
		
		protected function _reset()
		{
			$this->restriction = array();
        	$this->dbQuery->reset();
		}
        
        public function select()
        {
            $result = Db::getInstance()->executeS($this->dbQuery);
			$this->_reset();
			return $result;
        }
        
        public function getRow()
        {
            $result = Db::getInstance()->getRow($this->dbQuery);
			$this->_reset();
			return $result;
        }
        
        public function getValue($field)
        {
        	$this->dbQuery->select($field);
            $result = Db::getInstance()->getValue($this->dbQuery);
			$this->_reset();
			return $result;
        }
        
        public function getLastError() {
            return Db::getInstance()->getMsgError();
        }
        
        public function getLastSql() {
            return Db::getInstance()->getLastSql();
        }
        
        
        
        
        /*下面一系列方法只是转发给DdQuery类用来组织SQL语句*/
        
        //这里名称变了，不同于DdQuery类，用更符合习惯的fields，而select用于真正的查询动作
        public function fields($fields) {
            $this->dbQuery->select($fields);
            return $this;
        }
        
        public function from($table, $alias = null) {
            $this->dbQuery->from($table, $alias);
            return $this;
        }
        
        public function join($join) {
            $this->dbQuery->join($join);
            return $this;
        }
        
        public function leftJoin($table, $alias = null, $on = null) {
            $this->dbQuery->leftJoin($table, $alias, $on);
            return $this;
        }
        
        public function innerJoin($table, $alias = null, $on = null) {
            $this->dbQuery->innerJoin($table, $alias, $on);
            return $this;
        }
        
        public function leftOuterJoin($table, $alias = null, $on = null) {
            $this->dbQuery->leftOuterJoin($table, $alias, $on);
            return $this;
        }
        
        public function naturalJoin($table, $alias = null) {
            $this->dbQuery->naturalJoin($table, $alias);
            return $this;
        }
        
		protected function _checkArgs($args, $num) {
			array_shift($args);
			$params = $args;
			foreach ($params as $key => $value) {
				if (is_array($value)) 
					die(sprintf(Tools::displayError('The query argument should not be array if given more than 2.')));		
			}
			return $params;
		}
		
		/**
		 * add params to prepare sql
		 * @author update by david
		 */
        public function where($restriction, $params) {
			$num = func_num_args();
			if ($num > 2) { // 驗證多個參數的類型
				$args = func_get_args();
				$params = $this->_checkArgs($args, $num);
			}
			$sql = Db::getInstance()->prepare($restriction, $params);
            $this->dbQuery->where($restriction, $params);
            if (!empty($sql))
                $this->restriction[] = $sql;
            return $this;
        }
        
        public function having($restriction) {
            $this->dbQuery->having($restriction);
            return $this;
        }
        
        public function orderBy($fields) {
            $this->dbQuery->orderBy($fields);
            return $this;
        }
        
        public function groupBy($fields) {
            $this->dbQuery->groupBy($fields);
            return $this;
        }
        
        public function limit($offset = 0, $length=null) {
        	if(is_null($length) && strpos($offset,',')){
	            list($offset,$length)   =   explode(',',$offset);
	        }
			// update by david
			if (!$length) {
				$length = $offset;
				$offset = 0;
			}
            $this->dbQuery->limit($length, $offset);
            return $this;
        }
		
		/**
		 * support original sql
		 * @author add by david
		 * @param sql 
		 * @param args
		 * @return sql result
		 */
		public function query($query, $params)
		{
			$num = func_num_args();
			if ($num > 2) { // 驗證多個參數的類型
				$args = func_get_args();
				$params = $this->_checkArgs($args, $num);
			}
			$sql = ($params!==false) ? Db::getInstance()->prepare($query, $params) : $query;
	        $result = Db::getInstance()->executeS($sql);
			$this->_reset();
			return $result;
		}	
		
		/**
		 * @author add by david
		 * @return bool
		 */
		public function delete()
		{
			if (is_null($this->tablename)) return false;
            
            //禁止无条件的删除操作
            $where = ' 0 = 1 ';
            if ($this->restriction)
                $where = ' (' . implode(') AND (', $this->restriction) . ")\n";
            
			$limitArr = $this->dbQuery->getParam("limit");
			$orderArr = $this->dbQuery->getParam("order");
			$limit = 0;
			if (!empty($limitArr)) {
				$limit = isset($limitArr['limit']) ? $limitArr['limit'] : 0; 
				if (!empty($orderArr)) {
					$where .= 'ORDER BY ' . implode(', ', $orderArr);
				}
			}			
            $result = Db::getInstance()->delete($this->tablename, $where, $limit);
    
            $this->_reset();
			return $result;
		}	
		
		/**
		 * @author add by david
		 * @return array
		 */
		public function find()
		{
			$this->dbQuery->limit(1);
			$result = Db::getInstance()->executeS($this->dbQuery);
			$this->_reset();
			return (is_array($result) && !empty($result)) ? $result[0] : array(); 
		}
		
		public function setInc($field, $step=1)
		{
			if (!$field || !is_int($step)) return false;
			$data[$field] = array("type"=>"sql","value"=>"$field+$step");
			$result = $this->save($data);
			$this->_reset();
			return $result;
		}
		
		public function setDec($field, $step=1)
		{
			if (!$field || !is_int($step)) return false;
			$data[$field] = array("type"=>"sql","value"=>"$field-$step");
			$result = $this->save($data);
			$this->_reset();
			return $result;
		}
		
		public function __call($method, $args)
		{
			if(in_array(strtolower($method),array('count','sum','min','max','avg'),true)){
	            // 统计查询的实现
	            $field =  isset($args[0])?$args[0]:'*';
	            $count = $this->getValue(strtoupper($method).'('.$field.') AS ' . $method);
				return (int)$count;
	        }
			throw new Exception($method . " METHOD NOT EXIST");
            return;
		}
				
	    /**
	     * 获取数据表字段信息 update by david
	     * @access public
	     * @return array
	     */
	    protected function getDbFields(){
	        if(isset($this->tablename)) {// 动态指定表名
	            $table  =   $this->tablename;
                if(strpos($table,')')){
                    // 子查询
                    return false;
                }
	            $fields     =   Db::getInstance()->getFields($table);
				$this->fields   =   array_keys($fields);
        		unset($this->fields['_pk']);
        		$type = array();
				foreach ($fields as $key=>$val){
		            // 记录字段类型
		            $type[$key]     =   $val['type'];
		            if($val['primary']) {
		                  // 增加复合主键支持
		                if (isset($this->fields['_pk']) && $this->fields['_pk'] != null) {
		                    if (is_string($this->fields['_pk'])) {
		                        $this->pk   =   array($this->fields['_pk']);
		                        $this->fields['_pk']   =   $this->pk;
		                    }
		                    $this->pk[]   =   $key;
		                    $this->fields['_pk'][]   =   $key;
		                } else {
		                    $this->pk   =   $key;
		                    $this->fields['_pk']   =   $key;
		                }
		                if($val['autoinc']) $this->autoinc   =   true;
		            }
		        }
				 $this->fields['_type'] =  $type;
	            return  $fields ? array_keys($fields) : false;
	        }
	        return false;
	    }

		/**
	     * 对保存到数据库的数据进行处理  update by david
	     * @access protected
	     * @param mixed $data 要操作的数据
	     * @return boolean
	     */
	     protected function _facade($data) {
	     	$this->getDbFields();
	     	if(empty($this->fields)) return false;
	        // 检查数据字段合法性
	        $fields =   $this->fields;      
            foreach ($data as $key=>$val){
                if(!in_array($key,$fields,true)){
                    unset($data[$key]);
                }elseif(is_scalar($val)) {
                    // 字段类型检查 和 强制转换
                    $this->_parseType($data,$key);
                } elseif (is_array($val) && $val['type'] == "sql") {// setInc,setDec 數組方式
                	continue;
                } else {
                	unset($data[$key]);
                }
            }
	        return $data;
	     }

		/**
	     * 数据类型检测
	     * @access protected
	     * @param mixed $data 数据
	     * @param string $key 字段名
	     * @return void
	     */
	    protected function _parseType(&$data,$key) {
	    	$fields     =   Db::getInstance()->getFields($this->tablename);
	        if(isset($fields[$key])) {
	            $fieldType = strtolower($fields[$key]['type']);
	            if(false !== strpos($fieldType,'enum')){
	                if (false === strpos($fieldType, strtolower($data[$key]))) {
	                	throw new Exception("no such enum field");
						return;
	                }  
	            }elseif(false === strpos($fieldType,'bigint') && false !== strpos($fieldType,'int')) {
	                $data[$key]   =  intval($data[$key]);
	            }elseif(false !== strpos($fieldType,'float') || false !== strpos($fieldType,'double')){
	                $data[$key]   =  floatval($data[$key]);
	            }elseif(false !== strpos($fieldType,'bool')){
	                $data[$key]   =  (bool)$data[$key];
	            }
	        } else {
	        	unset($data[$key]);
	        }
	    }
		
		 /**
	     * 获取主键名称 upadte by david
	     * @access public
	     * @return string
	     */
	    public function getPk() {
	    	$this->getDbFields();
	        return $this->pk;
	    }
		
		/**
	     * 得到完整的数据表名
	     * @access public
	     * @return string
		 * @author davidhuang
	     */
	    public function getTableName() {
	    	if ($this->trueTableName) return $this->trueTableName;
	        return strtolower($this->tablename);
	    }
    }	
?>
    
