<?php
/**
 +------------------------------------------------------------------------------
 * 會話類
 +------------------------------------------------------------------------------
 * @author    davidhuang <davidhuang@nmg.com.hk>
 * @version   $Id$
 +------------------------------------------------------------------------------
 */
/**
 * 数据库方式Session驱动
 *    CREATE TABLE session (
 *      session_id varchar(255) NOT NULL,
 *      session_expire int(11) NOT NULL,
 *      session_data blob,
 *      UNIQUE KEY `session_id` (`session_id`)
 *    );
 */
class SessionDb {

	/**
	 * Session有效时间
	 */
	protected $lifeTime = '';
	
	protected $sessionConfig = "";
	
	protected $dbConfig = "";
	
	/**
	 * session保存的数据库名
	 */
	protected $sessionTable = '';

   /**
   * 数据库句柄
   */
   protected $hander; 
   
	/**
	 * 打开Session
	 * @access public
	 * @param string $savePath
	 * @param mixed $sessName
	 */
	public function open($savePath, $sessName) {
		$this->sessionConfig = $this->getSessionConfig();
		$this->dbConfig = $this->getDbConfig();
		$this->lifeTime = $this->sessionConfig['lifeTime'] ? $this->sessionConfig['lifeTime'] : ini_get('session.gc_maxlifetime');
		$this->sessionTable = $this->sessionConfig['sessionTable']  ? $this->sessionConfig['sessionTable'] : "session";
		//主数据库链接
       $hander = mysqli_connect( 
	           $this->dbConfig['host'],
	           $this->dbConfig['user'],
	           $this->dbConfig['password']
           );
   	   $dbSel = mysqli_select_db($hander, $this->dbConfig['db']);
	   if(!$hander || !$dbSel)  return false;
	   
	   $this->hander = $hander; 
		return true;
	}

	/**
	 * 关闭Session
	 * @access public
	 */
	public function close() {
		$this->gc($this->lifeTime);
		return  mysqli_close($this->hander); 
	}

	/**
	 * 读取Session
	 * @access public
	 * @param string $sessID
	 */
	public function read($sessID) {
		$res = mysqli_query($this->hander, "SELECT session_data AS data FROM ".$this->sessionTable." WHERE session_id = '$sessID'   AND session_expire >".time()); 
		if($res) {
        	$row = mysqli_fetch_assoc($res);
        }
		return $res ?  base64_decode($row['data']) : "";
	}

	/**
	 * 写入Session
	 * @access public
	 * @param string $sessID
	 * @param String $sessData
	 */
	public function write($sessID, $sessData) {
		$res = mysqli_query($this->hander, "SELECT session_expire as lifetime, session_data AS data FROM ".$this->sessionTable." WHERE session_id = '$sessID'   AND session_expire >".time()); 
		if($res) {
        	$row = mysqli_fetch_assoc($res);
        }
		$sessData = base64_encode($sessData);
		if ($res) {
			$currLifetime =  $row['lifetime'];
			$currSessData = $row['data'];
			$limitTime = 60*5; // 5分鐘
			//在5分鐘內數據未改變不更新DB
			if ($currSessData == $sessData && ($currLifetime - $this->lifeTime + $limitTime) > time()) {
				return true;
			}
		}
		$hander = $this->hander;
        $expire = time() + $this->lifeTime; 
        mysqli_query($hander, "REPLACE INTO  ".$this->sessionTable." (  session_id, session_expire, session_data)  VALUES( '$sessID', '$expire',  '$sessData')"); 		
		return mysqli_affected_rows($hander) ? true : false; 
	}

	/**
	 * 删除Session
	 * @access public
	 * @param string $sessID
	 */
	public function destroy($sessID) {
		$hander = $this->hander;
		mysqli_query($hander, "DELETE FROM ".$this->sessionTable." WHERE session_id = '$sessID'"); 
        if(mysqli_affected_rows($hander)) 
        	return true; 
		return false; 
	}

	/**
	 * Session 垃圾回收
	 * @access public
	 * @param string $sessMaxLifeTime
	 */
	public function gc($sessMaxLifeTime) {
		$rand = rand(0, 100);
		if ($rand != 50) return;
		$hander = $this->hander;
        mysqli_query($hander, "DELETE FROM ".$this->sessionTable." WHERE session_expire < ".time()); 
        return mysqli_affected_rows($hander); 
	}
	
	protected function getSessionConfig() {
		if (Yaf_Registry::has('session_config')) {
			return Yaf_Registry::get('session_config');
		} else {
			$sessionConfig = Yaf_Registry::get('config')->session;
			if (!empty($sessionConfig))	{
				$config = array('sessionTable' => $sessionConfig->sessionTable, 
								'lifeTime' => $sessionConfig->lifeTime, 
								'prefix'=>$sessionConfig->prefix);
								
				Yaf_Registry::set('session_config', $config);
			}
			return $config;
		}
	}
	
	private function getDbConfig()
	{
		$dbConfig = Yaf_Registry::get('config')->database->master;
		$config = array("host"=>$dbConfig->server,
						"user"=>$dbConfig->user,
						"password"=>$dbConfig->password,
						"db"=>$dbConfig->database);
		return $config;
	}
	
    /**
     * 打开Session 
     * @access public 
     */
    public function execute() {
        session_set_save_handler(array(&$this,"open"), 
                         array(&$this,"close"), 
                         array(&$this,"read"), 
                         array(&$this,"write"), 
                         array(&$this,"destroy"), 
                         array(&$this,"gc")); 
    }
	
}
?>