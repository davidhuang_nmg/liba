<?php
/**
 +------------------------------------------------------------------------------
 * 會話類
 +------------------------------------------------------------------------------
 * @author    davidhuang <davidhuang@nmg.com.hk>
 * @version   $Id$
 +------------------------------------------------------------------------------
 */
/**
 * Redis方式Session驱动
 */
class SessionRedis {

	/**
	 * Session有效时间
	 */
	protected $lifeTime = '';
	
	protected $sessionConfig = "";
	
	/**
	 * session保存的前綴名
	 */
	protected $namespace = '';

   /**
   * 数据库句柄
   */
   protected $hander; 
   
	/**
	 * 打开Session
	 * @access public
	 * @param string $savePath
	 * @param mixed $sessName
	 */
	public function open($savePath, $sessName) {
		$this->sessionConfig = $this->getSessionConfig();
		$this->lifeTime = $this->sessionConfig['lifeTime'] ? $this->sessionConfig['lifeTime'] : ini_get('session.gc_maxlifetime');
		$this->namespace = $this->sessionConfig['namespace']  ? $this->sessionConfig['namespace'] : "PHPSESSION:";
		//主数据库链接
	    $hander = Cache::getInstance();  
	    if(!$hander)  return false;
	    $this->hander = $hander; 
		return true;
	}

	/**
	 * 关闭Session
	 * @access public
	 */
	public function close() {
		$this->hander->close();
		return true; 
	}

	/**
	 * 读取Session
	 * @access public
	 * @param string $sessID
	 */
	public function read($sessID) {
		$sessID = $this->namespace . $sessID;
		$this->reconnect();
		$res = $this->hander->get($sessID);
		return $res ? base64_decode($res) : "";
	}

	/**
	 * 写入Session
	 * @access public
	 * @param string $sessID
	 * @param String $sessData
	 */
	public function write($sessID, $sessData) {
		$sessID = $this->namespace . $sessID;
		$sessData = base64_encode($sessData);
		$this->reconnect();
		return $this->hander->set($sessID, $sessData, $this->lifeTime);
	}
	
	protected function reconnect() {
		if (!$this->hander->is_connected) {
			$this->hander->connect();
		}
	}
	
	/**
	 * 删除Session
	 * @access public
	 * @param string $sessID
	 */
	public function destroy($sessID) {
		$this->reconnect();
		$sessID = $this->namespace . $sessID;
		return $this->hander->delete($sessID);
	}

	/**
	 * Session 垃圾回收
	 * @access public
	 * @param string $sessMaxLifeTime
	 */
	public function gc($sessMaxLifeTime) {
		return true;
	}
	
	protected function getSessionConfig() {
		if (Yaf_Registry::has('session_config')) {
			return Yaf_Registry::get('session_config');
		} else {
			$sessionConfig = Yaf_Registry::get('config')->session;
			if (!empty($sessionConfig))	{
				$config = array('lifeTime' => $sessionConfig->lifeTime, 
								'namespace'=>$sessionConfig->prefix);
								
				Yaf_Registry::set('session_config', $config);
			}
			return $config;
		}
	}
	
    /**
     * 打开Session 
     * @access public 
     */
    public function execute() {
        session_set_save_handler(array(&$this,"open"), 
                         array(&$this,"close"), 
                         array(&$this,"read"), 
                         array(&$this,"write"), 
                         array(&$this,"destroy"), 
                         array(&$this,"gc")); 
    }
}
?>